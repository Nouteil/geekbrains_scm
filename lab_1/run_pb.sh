#!/usr/bin/env bash


ansible-playbook -vv \
	-i inventory/prom/hosts.txt \
	-k -K \
	copytest.yml "$@"
